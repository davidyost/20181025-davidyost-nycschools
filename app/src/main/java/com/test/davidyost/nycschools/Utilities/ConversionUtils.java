package com.test.davidyost.nycschools.Utilities;

public class ConversionUtils {

    /**
     * Helper method for converting the string values from the JSON object to double values.  Since some
     * values go out many decimals we want to round it off and convert it to a more readable number
     * instead of just displaying the given value as a string.  If the parsing fails we will just return
     * a 0 and hide the pertinent data or indicate it is n/a.
     *
     * @param value - String parameter containing value to be parsed
     * @return double value containing the number from parsing, if parsing fails 0.0 is returned
     */
    public static double ConvertStringToDouble(String value) {
        double retval = 0.0;

        try {
            retval = Double.parseDouble(value);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  retval;
    }

    /**
     * Helper method specifically for conditionalizing formatting for any values that were retrieved from
     * the web api as null.  If the value is null we just pass back an empty string so the UI does not
     * blow a tire (like it would with a null value).
     * @param value - String parameter containing the value to be evaluated
     * @return String value with either a blank String or the value that was passed in
     */
    public static String GetDisplayValue(String value) {
        if (value == null)
            return "";
        else
            return value;
    }

}
