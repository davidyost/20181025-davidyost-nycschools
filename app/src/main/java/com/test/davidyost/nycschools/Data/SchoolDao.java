package com.test.davidyost.nycschools.Data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.test.davidyost.nycschools.Model.School;

import java.util.ArrayList;
import java.util.List;

public class SchoolDao {

    public static class SchoolEntry implements BaseColumns {
        public static final String TABLE_NAME = "schools";

        public static final String FIELD_ID = "id";
        public static final String FIELD_DBN = "dbn";
        public static final String FIELD_ATTENDANCE_RATE = "attendance_rate";
        public static final String FIELD_SCHOOL_NAME = "school_name";
        public static final String FIELD_ADDRESS = "address";
        public static final String FIELD_CITY = "city";
        public static final String FIELD_STATE = "state";
        public static final String FIELD_ZIP = "zip";
        public static final String FIELD_PHONE = "phone";
        public static final String FIELD_EMAIL = "email";
        public static final String FIELD_WEBSITE = "website";
        public static final String FIELD_LATITUDE = "latitude";
        public static final String FIELD_LONGITUDE = "longitude";
        public static final String FIELD_LOCATION = "location";
        public static final String FIELD_EXTRACURRICULAR_ACTIVITIES = "extracurricular_activities";
        public static final String FIELD_OVERVIEW_PARAGRAPH = "overview_paragraph";

        public static final String[] allFields = { FIELD_ID, FIELD_DBN, FIELD_ATTENDANCE_RATE, FIELD_SCHOOL_NAME, FIELD_ADDRESS,
                FIELD_CITY, FIELD_STATE, FIELD_ZIP, FIELD_PHONE, FIELD_EMAIL, FIELD_WEBSITE, FIELD_LATITUDE,
                FIELD_LONGITUDE, FIELD_LOCATION, FIELD_EXTRACURRICULAR_ACTIVITIES, FIELD_OVERVIEW_PARAGRAPH };
    }

    public static void deleteAllSchools(SQLiteDatabase db) {
        db.delete(SchoolEntry.TABLE_NAME, null, null);
    }

    public static long saveSchool(SQLiteDatabase db, School school) {
        ContentValues values = new ContentValues();
        values.put(SchoolEntry.FIELD_DBN, school.getDbn());
        values.put(SchoolEntry.FIELD_ATTENDANCE_RATE, school.getAttendanceRate());
        values.put(SchoolEntry.FIELD_SCHOOL_NAME, school.getSchoolName());
        values.put(SchoolEntry.FIELD_ADDRESS, school.getAddress());
        values.put(SchoolEntry.FIELD_CITY, school.getCity());
        values.put(SchoolEntry.FIELD_STATE, school.getState());
        values.put(SchoolEntry.FIELD_ZIP, school.getZip());
        values.put(SchoolEntry.FIELD_PHONE, school.getPhone());
        values.put(SchoolEntry.FIELD_EMAIL, school.getEmail());
        values.put(SchoolEntry.FIELD_WEBSITE, school.getWebsite());
        values.put(SchoolEntry.FIELD_LATITUDE, school.getLatitude());
        values.put(SchoolEntry.FIELD_LONGITUDE, school.getLongitude());
        values.put(SchoolEntry.FIELD_LOCATION, school.getLocation());
        values.put(SchoolEntry.FIELD_EXTRACURRICULAR_ACTIVITIES, school.getExtracurricularActivities() == null ? "" : TextUtils.join(", ", school.getExtracurricularActivities()));
        values.put(SchoolEntry.FIELD_OVERVIEW_PARAGRAPH, school.getOverviewParagraph());

        return db.insert(SchoolEntry.TABLE_NAME, null, values);
    }

    public static School getSchool(SQLiteDatabase db, long id) {
        String selection = SchoolEntry.FIELD_ID + " = ?";
        String[] selectionArgs = { Long.toString(id) };

        Cursor cursor = db.query(SchoolEntry.TABLE_NAME, SchoolEntry.allFields, selection, selectionArgs, null, null, null);

        if (cursor.moveToNext())
            return populateObject(cursor);

        return null;
    }

    public static List<School> getAllSchools(SQLiteDatabase db) {
        List<School> schoolList = new ArrayList<>();

        Cursor cursor = db.query(SchoolEntry.TABLE_NAME, SchoolEntry.allFields, null, null, null, null, null);

        while (cursor.moveToNext()) {
            schoolList.add(populateObject(cursor));
        }

        return schoolList;
    }

    public static School populateObject(Cursor cursor) {
        School school = new School();

        school.setId(cursor.getLong(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_ID)));
        school.setDbn(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_DBN)));
        school.setAttendanceRate(cursor.getDouble(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_ATTENDANCE_RATE)));
        school.setSchoolName(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_SCHOOL_NAME)));
        school.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_ADDRESS)));
        school.setCity(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_CITY)));
        school.setState(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_STATE)));
        school.setZip(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_ZIP)));
        school.setPhone(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_PHONE)));
        school.setEmail(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_EMAIL)));
        school.setWebsite(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_WEBSITE)));
        school.setLatitude(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_LATITUDE)));
        school.setLongitude(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_LONGITUDE)));
        school.setLocation(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_LOCATION)));
        school.setExtracurricularActivities(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_EXTRACURRICULAR_ACTIVITIES)).split(","));
        school.setOverviewParagraph(cursor.getString(cursor.getColumnIndexOrThrow(SchoolEntry.FIELD_OVERVIEW_PARAGRAPH)));

        return school;
    }
}
