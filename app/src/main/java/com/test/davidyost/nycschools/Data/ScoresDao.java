package com.test.davidyost.nycschools.Data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.test.davidyost.nycschools.Model.Scores;

import java.util.ArrayList;
import java.util.List;

public class ScoresDao {

    public static class ScoreEntry implements BaseColumns {
        public static final String TABLE_NAME = "scores";

        public static final String FIELD_ID = "id";
        public static final String FIELD_DBN = "dbn";
        public static final String FIELD_NUM_TEST_TAKERS = "num_test_takers";
        public static final String FIELD_AVG_READING_SCORE = "avg_reading_score";
        public static final String FIELD_AVG_MATH_SCORE = "avg_math_score";
        public static final String FIELD_AVG_WRITING_SCORE = "avg_writing_score";
        public static final String FIELD_SCHOOL_NAME = "school_name";

        public static final String[] allFields = { FIELD_ID, FIELD_DBN, FIELD_NUM_TEST_TAKERS, FIELD_AVG_READING_SCORE,
                FIELD_AVG_MATH_SCORE, FIELD_AVG_WRITING_SCORE, FIELD_SCHOOL_NAME };
    }

    public static void deleteAllScores(SQLiteDatabase db) {
        db.delete(ScoreEntry.TABLE_NAME, null, null);
    }

    public static long saveScores(SQLiteDatabase db, Scores scores) {
        ContentValues values = new ContentValues();
        values.put(ScoreEntry.FIELD_DBN, scores.getDbn());
        values.put(ScoreEntry.FIELD_NUM_TEST_TAKERS, scores.getNumTestTakers());
        values.put(ScoreEntry.FIELD_AVG_READING_SCORE, scores.getAvgReadingScore());
        values.put(ScoreEntry.FIELD_AVG_MATH_SCORE, scores.getAvgMathScore());
        values.put(ScoreEntry.FIELD_AVG_WRITING_SCORE, scores.getAvgWritingScore());
        values.put(ScoreEntry.FIELD_SCHOOL_NAME, scores.getSchoolName());

        return db.insert(ScoreEntry.TABLE_NAME, null, values);
    }

    public static Scores getScores(SQLiteDatabase db, long id) {
        String selection = ScoreEntry.FIELD_ID + " = ?";
        String[] selectionArgs = { Long.toString(id) };

        Cursor cursor = db.query(ScoreEntry.TABLE_NAME, ScoreEntry.allFields, selection, selectionArgs, null, null, null);

        if (cursor.moveToNext())
            return populateObject(cursor);

        return null;
    }

    public static Scores getScoresByDbn(SQLiteDatabase db, String dbn) {
        String selection = ScoreEntry.FIELD_DBN + " = ?";
        String[] selectionArgs = { dbn };

        Cursor cursor = db.query(ScoreEntry.TABLE_NAME, ScoreEntry.allFields, selection, selectionArgs, null, null, null);

        if (cursor.moveToNext())
            return populateObject(cursor);

        return null;
    }

    public static List<Scores> getAllScores(SQLiteDatabase db) {
        List<Scores> scoresList = new ArrayList<>();

        Cursor cursor = db.query(ScoreEntry.TABLE_NAME, SchoolDao.SchoolEntry.allFields, null, null, null, null, null);

        while (cursor.moveToNext()) {
            scoresList.add(populateObject(cursor));
        }

        return scoresList;
    }

    public static Scores populateObject(Cursor cursor) {
        Scores scores = new Scores();

        scores.setId(cursor.getLong(cursor.getColumnIndexOrThrow(ScoreEntry.FIELD_ID)));
        scores.setDbn(cursor.getString(cursor.getColumnIndexOrThrow(ScoreEntry.FIELD_DBN)));
        scores.setNumTestTakers(cursor.getString(cursor.getColumnIndexOrThrow(ScoreEntry.FIELD_NUM_TEST_TAKERS)));
        scores.setAvgReadingScore(cursor.getString(cursor.getColumnIndexOrThrow(ScoreEntry.FIELD_AVG_READING_SCORE)));
        scores.setAvgMathScore(cursor.getString(cursor.getColumnIndexOrThrow(ScoreEntry.FIELD_AVG_MATH_SCORE)));
        scores.setAvgWritingScore(cursor.getString(cursor.getColumnIndexOrThrow(ScoreEntry.FIELD_AVG_WRITING_SCORE)));
        scores.setSchoolName(cursor.getString(cursor.getColumnIndexOrThrow(ScoreEntry.FIELD_SCHOOL_NAME)));

        return scores;
    }
}
