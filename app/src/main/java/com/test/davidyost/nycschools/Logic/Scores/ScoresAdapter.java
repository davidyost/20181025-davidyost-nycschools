package com.test.davidyost.nycschools.Logic.Scores;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.davidyost.nycschools.Model.Scores;
import com.test.davidyost.nycschools.R;

import java.util.List;

public class ScoresAdapter extends RecyclerView.Adapter<ScoresViewHolder> {

    private List<Scores> mScores;

    public ScoresAdapter(List<Scores> scores) {
        mScores = scores;
    }

    @Override
    public ScoresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.score_item_layout, parent, false);
        ScoresViewHolder vh = new ScoresViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ScoresViewHolder holder, int position) {
        holder.mTVSchoolName.setText(mScores.get(position).getSchoolName());
    }

    @Override
    public int getItemCount() {
        return mScores == null ? 0 : mScores.size();
    }
}