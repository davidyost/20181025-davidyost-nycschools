package com.test.davidyost.nycschools.Logic.Scores;

import com.test.davidyost.nycschools.Model.Scores;

import java.util.List;

/**
 * This interface is used to indicate to the Activity that the scores list has been
 * retrieved from the web api and is updated in the database for use.  This allows
 * for a cleaner pass between the Async tasks and the calling Activity.
 */
public interface OnScoresRetrievedListener {
    void onScoresRetrieved(List<Scores> scoresList);
}
