package com.test.davidyost.nycschools.Logic.Scores;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.test.davidyost.nycschools.R;

public class ScoresViewHolder extends RecyclerView.ViewHolder {

    public TextView mTVSchoolName;

    public ScoresViewHolder(View v) {
        super(v);

        mTVSchoolName = v.findViewById(R.id.score_item_tv_school_name);
    }
}
