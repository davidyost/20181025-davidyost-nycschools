package com.test.davidyost.nycschools.Logic.Schools;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.davidyost.nycschools.Model.School;
import com.test.davidyost.nycschools.R;

import java.util.List;

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsViewHolder> {

    private SchoolsViewHolder.OnClickListener mOnClickListener;

    private List<School> mSchools;

    public SchoolsAdapter(List<School> schools, SchoolsViewHolder.OnClickListener onClickListener) {
        this.mSchools = schools;
        this.mOnClickListener = onClickListener;
    }

    @Override
    public SchoolsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_item_layout, parent, false);
        SchoolsViewHolder vh = new SchoolsViewHolder(v, mOnClickListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(SchoolsViewHolder holder, int position) {
        School school = mSchools.get(position);
        holder.mTVSchoolTitle.setText(school.getSchoolName());
        holder.mTVSchoolSubtitle.setText(school.getCity() + ", " + school.getState() + " " + school.getZip());
    }

    @Override
    public int getItemCount() {
        return mSchools == null ? 0 : mSchools.size();
    }

    public School getSchool(int position) {
        return mSchools.get(position);
    }
}