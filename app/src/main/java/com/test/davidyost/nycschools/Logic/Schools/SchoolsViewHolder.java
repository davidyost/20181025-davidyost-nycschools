package com.test.davidyost.nycschools.Logic.Schools;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.test.davidyost.nycschools.R;

public class SchoolsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private OnClickListener mOnClickListener;

    public TextView mTVSchoolTitle;
    public TextView mTVSchoolSubtitle;

    public SchoolsViewHolder(View v, OnClickListener onClickListener) {
        super(v);

        itemView.setOnClickListener(this);

        this.mOnClickListener = onClickListener;

        mTVSchoolTitle = v.findViewById(R.id.school_item_tv_title);
        mTVSchoolSubtitle = v.findViewById(R.id.school_item_tv_subtitle);
    }

    @Override
    public void onClick(View view) {
        mOnClickListener.onItemClick(getAdapterPosition(), view);
    }

    public interface OnClickListener {
        void onItemClick(int position, View view);
    }
}
