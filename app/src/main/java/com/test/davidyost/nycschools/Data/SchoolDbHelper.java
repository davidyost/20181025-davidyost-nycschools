package com.test.davidyost.nycschools.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SchoolDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "schools.db";
    private static final String SQL_CREATE_SCHOOLS_TABLE = "CREATE TABLE " + SchoolDao.SchoolEntry.TABLE_NAME + "(" +
            SchoolDao.SchoolEntry.FIELD_ID + " INTEGER PRIMARY KEY, " +
            SchoolDao.SchoolEntry.FIELD_DBN + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_ATTENDANCE_RATE + " REAL, " +
            SchoolDao.SchoolEntry.FIELD_SCHOOL_NAME + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_ADDRESS + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_CITY + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_STATE + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_ZIP + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_PHONE +  " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_EMAIL + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_WEBSITE + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_LATITUDE + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_LONGITUDE + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_LOCATION + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_EXTRACURRICULAR_ACTIVITIES + " TEXT, " +
            SchoolDao.SchoolEntry.FIELD_OVERVIEW_PARAGRAPH + " TEXT)";

    private static final String SQL_CREATE_SCORES_TABLE = "CREATE TABLE " + ScoresDao.ScoreEntry.TABLE_NAME + " (" +
            ScoresDao.ScoreEntry.FIELD_ID + " INTEGER PRIMARY KEY, " +
            ScoresDao.ScoreEntry.FIELD_DBN + " TEXT, " +
            ScoresDao.ScoreEntry.FIELD_NUM_TEST_TAKERS + " TEXT, " +
            ScoresDao.ScoreEntry.FIELD_AVG_READING_SCORE + " TEXT, " +
            ScoresDao.ScoreEntry.FIELD_AVG_MATH_SCORE + " TEXT, " +
            ScoresDao.ScoreEntry.FIELD_AVG_WRITING_SCORE + " TEXT, " +
            ScoresDao.ScoreEntry.FIELD_SCHOOL_NAME + " TEXT);";

    private static final String SQL_DELETE_SCHOOLS = "DROP TABLE Schools;";
    private static final String SQL_DELETE_SCORES = "DROP TABLE Scores;";

    public SchoolDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_SCHOOLS_TABLE);
        db.execSQL(SQL_CREATE_SCORES_TABLE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // since this is just a database for caching data we are going to just kill everything on upgrades
        db.execSQL(SQL_DELETE_SCHOOLS);
        db.execSQL(SQL_DELETE_SCORES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}