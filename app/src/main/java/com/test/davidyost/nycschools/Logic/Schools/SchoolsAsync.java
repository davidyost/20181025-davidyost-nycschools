package com.test.davidyost.nycschools.Logic.Schools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.test.davidyost.nycschools.Data.SchoolDao;
import com.test.davidyost.nycschools.Data.SchoolDbHelper;
import com.test.davidyost.nycschools.Model.School;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class SchoolsAsync extends AsyncTask<Void, Integer, List<School> > {

    private static final String school_scores_url = "https://data.cityofnewyork.us/resource/97mf-9njv.json";

    private ProgressBar mProgressBar;
    private OnSchoolsRetrievedListener mOnSchoolsRetrievedListener;
    private SQLiteDatabase mDb;

    public SchoolsAsync(Context context, ProgressBar progressBar, OnSchoolsRetrievedListener onSchoolsRetrievedListener)
    {
        this.mProgressBar = progressBar;
        this.mOnSchoolsRetrievedListener = onSchoolsRetrievedListener;
        this.mDb = new SchoolDbHelper(context).getWritableDatabase();
    }

    @Override
    protected void onPreExecute() {
        this.mProgressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    protected List<School> doInBackground(Void... params) {
        try {
            URL url = new URL(school_scores_url);
            HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();

            if (connection.getResponseCode() == 200) {
                InputStream inputStream = null;
                inputStream = connection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = bufferedReader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }

                JSONArray jsonArray = new JSONArray(sb.toString());
                List<School> schoolList = new ArrayList<>();
                int length = jsonArray.length();

                SchoolDao.deleteAllSchools(this.mDb);

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    School school = new School(jsonArray.getJSONObject(i));
                    school.setId(SchoolDao.saveSchool(this.mDb, school));
                    schoolList.add(school);

                    int progress = (i * 100) / length;
                    publishProgress(progress);
                }

                return schoolList;
            } else {
                // Error handling code goes here
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(List<School> schoolList) {
        if (mDb != null && mDb.isOpen())
            mDb.close();

        if (mOnSchoolsRetrievedListener != null)
            mOnSchoolsRetrievedListener.onSchoolsRetrievedListener(schoolList);

    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        this.mProgressBar.setProgress(values[0]);
    }
}
