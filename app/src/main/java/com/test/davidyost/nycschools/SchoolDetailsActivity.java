package com.test.davidyost.nycschools;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.test.davidyost.nycschools.Data.SchoolDao;
import com.test.davidyost.nycschools.Data.SchoolDbHelper;
import com.test.davidyost.nycschools.Data.ScoresDao;
import com.test.davidyost.nycschools.Model.School;
import com.test.davidyost.nycschools.Model.Scores;
import com.test.davidyost.nycschools.Utilities.ConversionUtils;

public class SchoolDetailsActivity extends AppCompatActivity {

    SchoolDbHelper mDbHelper;
    School mSchool;
    Scores mScores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling_school_details);

        this.setTitle("School Details");

        Toolbar toolbar = (Toolbar) findViewById(R.id.school_details_toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabCall = findViewById(R.id.school_details_fab_call);
        fabCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = mSchool.getPhone().replaceAll("[^\\d.]", "");
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phone));

                if (ActivityCompat.checkSelfPermission(SchoolDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                startActivity(callIntent);
            }
        });


        mDbHelper = new SchoolDbHelper(getBaseContext());
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        long schoolId = getIntent().getLongExtra("SCHOOL_ID", 0);

        this.mSchool = SchoolDao.getSchool(db, schoolId);
        this.mScores = ScoresDao.getScoresByDbn(db, this.mSchool.getDbn());

        TextView tvSchoolName = findViewById(R.id.school_details_tv_school_name);
        TextView tvSchoolAddress = findViewById(R.id.school_details_tv_school_address);

        tvSchoolName.setText(this.mSchool.getSchoolName());
        tvSchoolAddress.setText(this.mSchool.getAddress() + " " + this.mSchool.getCity() + ", " + this.mSchool.getState() + " " + this.mSchool.getZip());

        if (this.mScores != null) {
            TextView tvSatMath = findViewById(R.id.school_details_tv_sat_math);
            TextView tvSatReading = findViewById(R.id.school_details_tv_sat_reading);
            TextView tvSatWriting = findViewById(R.id.school_details_tv_sat_writing);
            TextView tvNumTestTakers = findViewById(R.id.school_details_tv_test_takers);

            tvSatMath.setText(ConversionUtils.GetDisplayValue(mScores.getAvgMathScore()));
            tvSatReading.setText(ConversionUtils.GetDisplayValue(mScores.getAvgReadingScore()));
            tvSatWriting.setText(ConversionUtils.GetDisplayValue(mScores.getAvgWritingScore()));
            tvNumTestTakers.setText(ConversionUtils.GetDisplayValue(mScores.getNumTestTakers()));
        }

        TextView tvOverviewParagraph = findViewById(R.id.school_details_tv_overview_paragraph);

        tvOverviewParagraph.setText(ConversionUtils.GetDisplayValue(mSchool.getOverviewParagraph()));
    }

    public void openWebSite(View view) {
        String url = this.mSchool.getWebsite();
        if (!this.mSchool.getWebsite().startsWith("http://"))
            url = "http://" + this.mSchool.getWebsite();

        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void sendEmail(View view) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",mSchool.getEmail(), null));
        startActivity(Intent.createChooser(intent, "Choose an Email client :"));
    }

    @Override
    protected void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }
}
