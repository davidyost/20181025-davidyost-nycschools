package com.test.davidyost.nycschools;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.davidyost.nycschools.Logic.Schools.SchoolsAsync;
import com.test.davidyost.nycschools.Logic.Schools.OnSchoolsRetrievedListener;
import com.test.davidyost.nycschools.Logic.Scores.ScoresAsync;
import com.test.davidyost.nycschools.Logic.Scores.OnScoresRetrievedListener;
import com.test.davidyost.nycschools.Model.School;
import com.test.davidyost.nycschools.Model.Scores;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnSchoolsRetrievedListener, OnScoresRetrievedListener {

    TextView mTvProgress;
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hideSystemUI();

        mTvProgress = findViewById(R.id.splash_tv_progress);
        mProgressBar = findViewById(R.id.splash_pb_progress);


        mProgressBar.setProgress(0);
        mTvProgress.setText("Retrieveing latest NYC School directory...");

        new SchoolsAsync(this, this.mProgressBar, this).execute();
    }

    @Override
    public void onSchoolsRetrievedListener(final List<School> schools) {
        mProgressBar.setProgress(0);
        mTvProgress.setText("Retrieveing latest SAT scores...");

        new ScoresAsync(this, this.mProgressBar, this).execute();
    }

    @Override
    public void onScoresRetrieved(List<Scores> scoresList) {
        startActivity(new Intent(getBaseContext(), SchoolListActivity.class));
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

}
