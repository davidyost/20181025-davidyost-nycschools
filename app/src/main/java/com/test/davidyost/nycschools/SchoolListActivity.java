package com.test.davidyost.nycschools;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.davidyost.nycschools.Data.SchoolDao;
import com.test.davidyost.nycschools.Data.SchoolDbHelper;
import com.test.davidyost.nycschools.Logic.Schools.SchoolsAdapter;
import com.test.davidyost.nycschools.Logic.Schools.SchoolsAsync;
import com.test.davidyost.nycschools.Logic.Schools.SchoolsViewHolder;
import com.test.davidyost.nycschools.Logic.Schools.OnSchoolsRetrievedListener;
import com.test.davidyost.nycschools.Logic.Scores.ScoresAsync;
import com.test.davidyost.nycschools.Logic.Scores.OnScoresRetrievedListener;
import com.test.davidyost.nycschools.Model.School;
import com.test.davidyost.nycschools.Model.Scores;

import java.util.List;

public class SchoolListActivity extends AppCompatActivity implements OnSchoolsRetrievedListener, OnScoresRetrievedListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private TextView mTvProgress;
    private ProgressBar mProgressBar;

    private SchoolDbHelper mDbHelper;
    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_list);

        this.setTitle("NYC School Directory");

        mDbHelper = new SchoolDbHelper(getBaseContext());
        mDb = mDbHelper.getWritableDatabase();

        mRecyclerView = findViewById(R.id.recycler_schools);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mTvProgress = findViewById(R.id.school_list_tv_progress);
        mProgressBar = findViewById(R.id.school_list_pb_progress);

        LoadSchools();
    }

    private void LoadSchools() {
        List<School> schools = SchoolDao.getAllSchools(mDb);
        mAdapter = new SchoolsAdapter(schools, new SchoolsViewHolder.OnClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                Intent intent = new Intent(getBaseContext(), SchoolDetailsActivity.class);
                intent.putExtra("SCHOOL_ID", ((SchoolsAdapter)mAdapter).getSchool(position).getId());
                startActivity(intent);
            }
        });

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onSchoolsRetrievedListener(final List<School> schools) {
        mProgressBar.setProgress(0);

        new ScoresAsync(this, mProgressBar, this).execute();
    }

    @Override
    public void onScoresRetrieved(List<Scores> scoresList) {
        mRecyclerView.setVisibility(View.VISIBLE);
        mTvProgress.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);

        LoadSchools();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_school_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_reload_data) {
            mRecyclerView.setVisibility(View.GONE);
            mTvProgress.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setProgress(0);

            new SchoolsAsync(this, mProgressBar, this).execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }
}
