package com.test.davidyost.nycschools.Logic.Schools;

import com.test.davidyost.nycschools.Model.School;

import java.util.List;

/**
 * This interface is used to indicate to the Activity that the school directory has been
 * retrieved from the web api and is updated in the database for use.  This allows
 * for a cleaner pass between the Async tasks and the calling Activity.
 */
public interface OnSchoolsRetrievedListener {
    void onSchoolsRetrievedListener(List<School> scoresList);
}
