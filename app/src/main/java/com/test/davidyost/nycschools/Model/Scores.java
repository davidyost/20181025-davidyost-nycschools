package com.test.davidyost.nycschools.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class Scores {

    private long id;
    private String dbn;
    private String numTestTakers;
    private String avgReadingScore;
    private String avgMathScore;
    private String avgWritingScore;
    private String schoolName;

    public Scores() {

    }

    public Scores(JSONObject jsonObject) {
        try {
            this.dbn = jsonObject.getString("dbn");
            this.numTestTakers = jsonObject.getString("num_of_sat_test_takers");
            this.avgReadingScore = jsonObject.getString("sat_critical_reading_avg_score");
            this.avgMathScore = jsonObject.getString("sat_math_avg_score");
            this.avgWritingScore = jsonObject.getString("sat_writing_avg_score");
            this.schoolName = jsonObject.getString("school_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Scores(String dbn, String numTestTakers, String avgReadingScore, String avgMathScore, String avgWritingScore, String schoolName) {
        this.dbn = dbn;
        this.numTestTakers = numTestTakers;
        this.avgReadingScore = avgReadingScore;
        this.avgMathScore = avgMathScore;
        this.avgWritingScore = avgWritingScore;
        this.schoolName = schoolName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getNumTestTakers() {
        return numTestTakers;
    }

    public void setNumTestTakers(String numTestTakers) {
        this.numTestTakers = numTestTakers;
    }

    public String getAvgReadingScore() {
        return avgReadingScore;
    }

    public void setAvgReadingScore(String avgReadingScore) {
        this.avgReadingScore = avgReadingScore;
    }

    public String getAvgMathScore() {
        return avgMathScore;
    }

    public void setAvgMathScore(String avgMathScore) {
        this.avgMathScore = avgMathScore;
    }

    public String getAvgWritingScore() {
        return avgWritingScore;
    }

    public void setAvgWritingScore(String avgWritingScore) {
        this.avgWritingScore = avgWritingScore;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
}
