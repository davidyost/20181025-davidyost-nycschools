package com.test.davidyost.nycschools.Logic.Scores;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.test.davidyost.nycschools.Data.SchoolDbHelper;
import com.test.davidyost.nycschools.Data.ScoresDao;
import com.test.davidyost.nycschools.Model.Scores;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class ScoresAsync extends AsyncTask<Void, Integer, List<Scores>> {

    private static final String school_scores_url = "https://data.cityofnewyork.us/resource/734v-jeq5.json";

    private ProgressBar mProgressBar;
    private OnScoresRetrievedListener mOnScoresRetrievedListener;
    private SQLiteDatabase mDb;

    public ScoresAsync(Context context, ProgressBar progressBar, OnScoresRetrievedListener onScoresRetrievedListener)
    {
        this.mProgressBar = progressBar;
        this.mOnScoresRetrievedListener = onScoresRetrievedListener;
        this.mDb = new SchoolDbHelper(context).getWritableDatabase();
    }

    @Override
    protected void onPreExecute() {
        this.mProgressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    protected List<Scores> doInBackground(Void... params) {
        try {
            URL url = new URL(school_scores_url);
            HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();

            if (connection.getResponseCode() == 200) {
                InputStream inputStream = null;
                inputStream = connection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = bufferedReader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }

                JSONArray jsonArray = new JSONArray(sb.toString());
                List<Scores> scoresList = new ArrayList<>();
                int length = jsonArray.length();

                ScoresDao.deleteAllScores(this.mDb);

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    Scores scores = new Scores(jsonArray.getJSONObject(i));
                    scores.setId(ScoresDao.saveScores(this.mDb, scores));
                    scoresList.add(scores);

                    int progress = (i * 100) / length;
                    publishProgress(progress);
                }

                return scoresList;
            } else {
                // Error handling code goes here
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(List<Scores> scoresList) {
        if (mDb != null && mDb.isOpen())
            mDb.close();

        if (mOnScoresRetrievedListener != null)
            mOnScoresRetrievedListener.onScoresRetrieved(scoresList);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        this.mProgressBar.setProgress(values[0]);
    }
}