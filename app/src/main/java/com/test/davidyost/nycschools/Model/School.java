package com.test.davidyost.nycschools.Model;

import com.test.davidyost.nycschools.Utilities.ConversionUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class School {

    private long id;
    private String dbn;
    private double attendanceRate;

    private String schoolName;
    private String address;
    private String city;
    private String state;
    private String zip;

    private String phone;
    private String email;
    private String website;

    private String latitude;
    private String longitude;
    private String location;

    private String[] extracurricularActivities;
    private String overviewParagraph;

    public School() {

    }

    public School(JSONObject jsonObject) {

        try {
            this.dbn = jsonObject.getString("dbn");
            this.attendanceRate = ConversionUtils.ConvertStringToDouble(jsonObject.getString("attendance_rate"));
            this.schoolName = jsonObject.getString("school_name");
            this.address = jsonObject.getString("primary_address_line_1");
            this.city = jsonObject.getString("city");
            this.state = jsonObject.getString("state_code");
            this.zip = jsonObject.getString("zip");
            this.phone = jsonObject.getString("phone_number");
            this.email = jsonObject.getString("school_email");
            this.website = jsonObject.getString("website");
            this.latitude = jsonObject.getString("latitude");
            this.longitude = jsonObject.getString("longitude");
            this.location = jsonObject.getString("location");
            this.extracurricularActivities = jsonObject.getString("extracurricular_activities").split(", ");
            this.overviewParagraph = jsonObject.getString("overview_paragraph");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public School(String dbn, double attendanceRate, String schoolName, String address, String city, String state, String zip, String phone, String email, String website, String latitude, String longitude, String location, String[] extracurricularActivities, String overviewParagraph) {
        this.dbn = dbn;
        this.attendanceRate = attendanceRate;
        this.schoolName = schoolName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phone = phone;
        this.email = email;
        this.website = website;
        this.latitude = latitude;
        this.longitude = longitude;
        this.location = location;
        this.extracurricularActivities = extracurricularActivities;
        this.overviewParagraph = overviewParagraph;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public double getAttendanceRate() {
        return attendanceRate;
    }

    public void setAttendanceRate(double attendanceRate) {
        this.attendanceRate = attendanceRate;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String[] getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public void setExtracurricularActivities(String[] extracurricularActivities) {
        this.extracurricularActivities = extracurricularActivities;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }
}
